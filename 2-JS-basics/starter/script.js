/*****************************
* Function Statements and Expressions
*/
/*
// Function declaration
// function whatDoYouDo(job, firstName) {}

// Function expression
var whatDoYouDo = function(job, firstName) {
    switch(job) {
        case 'teacher':
            return firstName + ' teaches kids how to code';
        case 'driver':
            return firstName + ' drives a cab in Lisbon.'
        case 'designer':
            return firstName + ' designs beautiful websites';
        default:
            return firstName + ' does something else';
    }
}

console.log(whatDoYouDo('teacher', 'John'));
console.log(whatDoYouDo('designer', 'Jane'));
console.log(whatDoYouDo('retired', 'Mark'));

https://medium.com/@mandeep1012/function-declarations-vs-function-expressions-b43646042052
Function declarations load before any code is executed while Function expressions load only when the interpreter reaches that line of code.

Benefits of Function Expressions
There are several different ways that function expressions become more useful than function declarations.

As closures - https://javascriptissexy.com/understand-javascript-closures-with-ease/
As arguments to other functions -
As Immediately Invoked Function Expressions (IIFE) - https://developer.mozilla.org/en-US/docs/Glossary/IIFE

Other Source: https://developer.mozilla.org/en-US/docs/web/JavaScript/Reference/Operators/function
https://medium.com/better-programming/newbie-js-function-declaration-vs-function-expression-a3ae67573270

*/

/************ ARRAYS ****************/



/*****************************
* CODING CHALLENGE 4
*/

/*
Let's remember the first coding challenge where Mark and John compared their BMIs. Let's now implement the same functionality with objects and methods.
1. For each of them, create an object with properties for their full name, mass, and height
2. Then, add a method to each object to calculate the BMI. Save the BMI to the object and also return it from the method.
3. In the end, log to the console who has the highest BMI, together with the full name and the respective BMI. Don't forget they might have the same BMI.

Remember: BMI = mass / height^2 = mass / (height * height). (mass in kg and height in meter).

GOOD LUCK 😀
*/

var mark = {
    fullName: 'Mark Jacobs',
    mass: 80, //176lbs
    height: 1.7, //5.7
    bmiCalc: function() {
        this.bmi = this.mass / (this.height * this.height);
        return this.bmi;
    }
};

var john = {
    fullName: 'John Jacobs',
    mass: 77, // 170lbs in kg
    height: 1.8, //5.9feet in meters
    bmiCalc: function() {
        this.bmi = this.mass / (this.height * this.height);
        return this.bmi;
    }
};

// mark.bmiCalc();
// john.bmiCalc();

console.log(mark.bmiCalc());

if (mark.bmiCalc() > john.bmiCalc()) {
    console.log(mark.fullName + ' has higher bmi');
} else if (john.bmi > mark.bmi) {
    console.log(john.fullName + ' has higher bmi');
} else {
    console.log('Their equal bmi');
}
/*

/******** Loops ******

// var john = ['john', 'smith', 1990, 'designer', false, 'blue'];

// for (var i = john.length - 1; i >= 0; i--){
//     console.log(john[i]);
// }

/*****************************
* CODING CHALLENGE 5
*/

/*
Remember the tip calculator challenge? Let's create a more advanced version using everything we learned!

This time, John and his family went to 5 different restaurants. The bills were $124, $48, $268, $180 and $42.
John likes to tip 20% of the bill when the bill is less than $50, 15% when the bill is between $50 and $200, and 10% if the bill is more than $200.

Implement a tip calculator using objects and loops:
1. Create an object with an array for the bill values
2. Add a method to calculate the tip
3. This method should include a loop to iterate over all the paid bills and do the tip calculations
4. As an output, create 1) a new array containing all tips, and 2) an array containing final paid amounts (bill + tip). HINT: Start with two empty arrays [] as properties and then fill them up in the loop.


EXTRA AFTER FINISHING: Mark's family also went on a holiday, going to 4 different restaurants. The bills were $77, $375, $110, and $45.
Mark likes to tip 20% of the bill when the bill is less than $100, 10% when the bill is between $100 and $300, and 25% if the bill is more than $300 (different than John).

5. Implement the same functionality as before, this time using Mark's tipping rules
6. Create a function (not a method) to calculate the average of a given array of tips. HINT: Loop over the array, and in each iteration store the current sum in a variable (starting from 0). After you have the sum of the array, divide it by the number of elements in it (that's how you calculate the average)
7. Calculate the average tip for each family
8. Log to the console which family paid the highest tips on average

GOOD LUCK 😀
*/
// var bill = [124, 48, 268, 180, 42];
// var tips = [];
// var fBill = [];

// function calc() {
//     return tips[i] + bill[i];
// };

// for (var i = 0; i < bill.length; i++ ){
//     var keep = bill[i]
//     if (keep < 50) {
//         tips.push (keep * 0.2);
//         fBill.push (calc());
//     } else if (keep > 50 && keep < 200) {
//         tips.push (keep * 0.15);
//         fBill.push (calc());
//     } else {
//         tips.push (keep * 0.1);
//         fBill.push (calc());
//     }
// }

// console.log (tips, fBill);

//FIX

var johnBill = {
    name : 'John',
    lastName : 'Smith',
    bill : [124, 48, 268, 180, 42],
    tip: [],
    fBill: [],
    tipsCalc: function(){
        for (var i = 0; i < this.bill.length; i++) {
        var keep = this.bill[i];
            if (keep < 50) {
                this.tip[i] =  keep * 0.2;
            } else if (keep > 50 && keep < 200) {
                this.tip[i] =  keep * 0.15;
            } else {
                this.tip[i] =  keep * 0.1;
            }
            this.fBill[i] = this.tip[i] + this.bill[i];
        }
    }
}

var markBill = {
    name : 'Mark',
    lastName : 'Samsung',
    bill : [77, 375, 110, 45],
    tip: [],
    fBill: [],
    tipsCalc: function(){
        for (var i = 0; i < this.bill.length; i++) {
        var keep = this.bill[i];
            if (keep < 100) {
                this.tip[i] =  keep * 0.2;
            } else if (keep > 100 && keep < 300) {
                this.tip[i] =  keep * 0.1;
            } else {
                this.tip[i] =  keep * 0.25;
            }
            this.fBill[i] = this.tip[i] + this.bill[i];
        }
    }
}

johnBill.tipsCalc(); //fill the array
markBill.tipsCalc(); //fill the array
// console.log (johnBill,markBill);

function avgBill(a){
    var sum = 0;
    for (var i=0; i < a.length; i++){
        sum += a[i];
    }
    sum = sum / a.length;
    return sum;
};

var johnsAvg = avgBill(johnBill.tip);
var marksAvg = avgBill(markBill.tip);
console.log(johnsAvg);
console.log(marksAvg);

if (johnsAvg > marksAvg){
    console.log('loser ' + ' johnsAvg is bigger. His average is ' + johnsAvg + ' while marksAvg is ' + marksAvg);
} else if (marksAvg > johnsAvg){
    console.log('loser ' + ' marksAvg is bigger. His average is ' + marksAvg + ' while johnsAvg is ' + johnsAvg);
} else {
    console.log ('Their the same duh');
}



