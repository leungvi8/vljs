/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, gamePlaying, lastDice, lastDice2, winningScore, captureScore;

winningScore = 10;

init();
// alert ('Game Rules - Player 1 goes first. On a six sided dice be the first get a total score of 100. Do not get greedy, hitting a number ONE makes it your opponents turn, so make sure you hit hold when you want to stop');



// document.querySelector('#current-' + activePlayer).textContent = dice; //setting a value
// //document.querySelector('#current-' + activePlayer).innerHTML = '<em>' + dice + '</em';
// //the above will spit out the number with some italics

// var x = document.querySelector('#score-0').textContent; //getting a value



//EVENTS - listener

// document.querySelector('.button-roll').addEventListener('click', btn); //function button without parathesis - called by another function

//or anonomous function - BELOW

document.querySelector('.btn-roll').addEventListener('click', function(){
	if(gamePlaying){

		var dice, dice2, diceSum;

		// 1. Random Number
		dice = Math.floor(Math.random() * 6) + 1;
		dice2 = Math.floor(Math.random() * 6) + 1;


		// 2. Display the result for Dice 1
		var diceDom = document.querySelector('.dice');
		diceDom.style.display = 'block';
		diceDom.src = 'dice-' +  dice + '.png';

		// 2. Display the result for Dice 2
		var diceDom2 = document.querySelector('.dice2');
		diceDom2.style.display = 'block';
		diceDom2.src = 'dice-' +  dice2 + '.png';
			console.log(dice + '' + dice2);
		// 3a. Update the player score if there is a 6
		if (((dice === lastDice) && (dice === 6)) || ((dice2 === lastDice2) && (dice2 === 6)) || ((dice === dice2) && (dice2===6))) { //3 EQUALS
			console.log('triggered RESET');
			scores[activePlayer] = 0; //VERY IMPORTANT !
			document.getElementById('score-' + activePlayer).textContent = '0';

			nextPlayer();
			dice = 0;
			dice2 = 0;


		// 3. Update the round score IF the rolled number is not 1
		} else if ((dice !== 1) && (dice2 !== 1)) { //type coercion
			//Add Score
			diceSum = dice + dice2;
			roundScore += diceSum;
			document.querySelector('#current-' + activePlayer).textContent = roundScore;


		} else {
			nextPlayer();
			lastDice = 0;
			lastDice2 = 0;

		}
		lastDice = dice;
		lastDice2 = dice2;

	}
});

document.querySelector('.btn-hold').addEventListener('click', function(){
	if (gamePlaying){
		//Add the current score to global score
		scores[activePlayer] += roundScore;

		//Update the UI
		document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];


		//Check if the player won the game
		if (scores[activePlayer] >= winningScore){
			document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
			document.querySelector('.dice').style.display = ('none');
			document.querySelector('.dice2').style.display = ('none');
			document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
			document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
			gamePlaying = false;

		} else {

		nextPlayer();

		}
	}

});

function nextPlayer (){

		//Next Player
		//terinary operator
		activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
		roundScore = 0;

		document.getElementById('current-0').textContent = '0';
		document.getElementById('current-1').textContent = '0';
		document.querySelector('.player-0-panel').classList.toggle('active');
		document.querySelector('.player-1-panel').classList.toggle('active');

		document.querySelector('.dice').style.display='none';
		document.querySelector('.dice2').style.display='none';

};

document.querySelector('.gameSubmit').addEventListener('click', function(){

	captureScore = document.getElementById("gameScore").value;
	console.log(captureScore);
	winningScore = captureScore;
	document.querySelector('.winningScore').textContent = winningScore;
	init();


});

// document.querySelector('.btn-new').addEventListener('click', function(){

// 	init();


// });

//instead of above initize directly after the click

document.querySelector('.btn-new').addEventListener('click',init); //no bracket or else it inititize immediatly

function init(){
	scores = [0,0];
	roundScore = 0;
	activePlayer = 0;

	captureScore = winningScore;

	document.querySelector('.dice').style.display = 'none';
	document.querySelector('.dice2').style.display = 'none';
	document.getElementById('score-0').textContent = '0';
	document.getElementById('score-1').textContent = '0';
	document.getElementById('current-0').textContent = '0';
	document.getElementById('current-1').textContent = '0';

	document.getElementById('name-0').textContent = 'Player 1';
	document.getElementById('name-1').textContent = 'Player 2';

	document.querySelector('.player-0-panel').classList.remove('winner');
	document.querySelector('.player-1-panel').classList.remove('winner');
	document.querySelector('.player-0-panel').classList.remove('active');
	document.querySelector('.player-1-panel').classList.remove('active');
	document.querySelector('.player-0-panel').classList.add('active');

	document.querySelector('.winningScore').textContent = winningScore;
	gamePlaying = true;
	lastDice = 0;
	lastDice2 = 0;


};



/*
YOUR 3 CHALLENGES
Change the game to follow these rules:

1. A player looses his ENTIRE score when he rolls two 6 in a row. After that, it's the next player's turn. (Hint: Always save the previous dice roll in a separate variable)
2. Add an input field to the HTML where players can set the winning score, so that they can change the predefined score of 100. (Hint: you can read that value with the .value property in JavaScript. This is a good oportunity to use google to figure this out :)
3. Add another dice to the game, so that there are two dices now. The player looses his current score when one of them is a 1. (Hint: you will need CSS to position the second dice, so take a look at the CSS code for the first one.)
*/