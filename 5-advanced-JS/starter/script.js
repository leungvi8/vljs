

/**** Bind, Call and Apply ***/

var vince = {
	name: 'Vince',
	age: 33,
	job: 'developer',
	presentation: function(style,time){
		if (style === 'formal'){
			console.log('Hello, my name is ' + this.name + ' I\'m ' + this.age + ' years old and I work as a ' + this.job + ' ' + time);
		} else if (style === 'lazy'){
			console.log('What up homie, name is ' + this.name + ' I\'m ' + this.age + ' years old and I work as a ' + this.job + ' ' + time);
		}
	}
};

var roxanne = {
	name: 'Roxanne',
	age: 29,
	job: 'streamer'
};

vince.presentation('lazy', 'afternoon');

vince.presentation.call(roxanne,'formal','morning');
//to borrow the method from another object use 'call' method

//Now just to show what is the apple method see below, this won't work in our above example but wanted to see how it would be called
// john.presentation.apply(emily, ['friendly', 'afternoon']);

//THIRD method is the bind method see below example
var vinceFriendly = vince.presentation.bind(vince, 'lazy'); //setting first argument here
vinceFriendly('morning'); //than here the other argument
vinceFriendly('evening'); //Here another time of the day!

/////////////////////////////
// CODING CHALLENGE


/*
--- Let's build a fun quiz game in the console! ---

1. Build a function constructor called Question to describe a question. A question should include:
a) question itself
b) the answers from which the player can choose the correct one (choose an adequate data structure here, array, object, etc.)
c) correct answer (I would use a number for this)

2. Create a couple of questions using the constructor

3. Store them all inside an array

4. Select one random question and log it on the console, together with the possible answers (each question should have a number) (Hint: write a method for the Question objects for this task).

5. Use the 'prompt' function to ask the user for the correct answer. The user should input the number of the correct answer such as you displayed it on Task 4.

6. Check if the answer is correct and print to the console whether the answer is correct ot nor (Hint: write another method for this).

7. Suppose this code would be a plugin for other programmers to use in their code. So make sure that all your code is private and doesn't interfere with the other programmers code (Hint: we learned a special technique to do exactly that).
*/

//Use IIFE to Privitize
(function () {

	loop(); //initial loop call


	function trigger(){ //trigger function for question and answers
		var Question = function (question, response, answer){
			this.question = question;
			this.response = response;
			this.answer = answer;
		}
		var guyQuest = new Question ('Am I a guy?', ['0:yes','1:no'], 0);
		var codeQuest = new Question ('Who wrote this code?', ['0:John','1:Kitty','2:Vince'], 2);
		var livingQuest = new Question ('What game genre do I enjoy the most?', ['0:RPG','1:FPS','2:Simulation','3:MMORPG'], 1);
		var allQuestions = [guyQuest, codeQuest, livingQuest];

		function getNum(min, max) {
		  return Math.floor(Math.random() * (max - min) + min);
		};

		var randomChoice = allQuestions[getNum(0,3)];

		console.log(randomChoice.question);

		for (var i = 0; i<randomChoice.response.length; i++){
			console.log(randomChoice.response[i]);
		}
		console.log(randomChoice);
		return randomChoice;

	}

	function loop(){ //Loop function to play the game

		var choice = trigger();

		var userAnswer = prompt("Answer the question below loser:");

		if(userAnswer === 'exit'){
			console.log('Thanks for playing');
		}

		else if(userAnswer == choice.answer){ //note this is 2 equals because prompt is a string, while the object is a number
			var userScore === 0;
			console.log('Ding Ding Correct');
			console.log(userScore);
			userScore = userScore + 1;


			loop();

		} else {
			console.log('ERRR... WRONG, try again!');
			loop();
		};

	}


})();


/*
--- Expert level ---

8. After you display the result, display the next random question, so that the game never ends (Hint: write a function for this and call it right after displaying the result)

9. Be careful: after Task 8, the game literally never ends. So include the option to quit the game if the user writes 'exit' instead of the answer. In this case, DON'T call the function from task 8.

10. Track the user's score to make the game more fun! So each time an answer is correct, add 1 point to the score (Hint: I'm going to use the power of closures for this, but you don't have to, just do this with the tools you feel more comfortable at this point).

11. Display the score in the console. Use yet another method for this.
*/