/*** Let's Create our own Objects and functions - Advanced JS - Section 5 ****/

//Beginning with Simple Object

// var nintendo = { //object literal
// 	name: 'Nintendo',
// 	countryOfOrigin: 'Japan',
// 	birthDate: 1889
// };

var Console = function(name,countryOfOrigin,birthDate){ //function contructor **NOTE About the Capital to signify it is a function constructor

	this.name = name;
	this.countryOfOrigin = countryOfOrigin;
	this.birthDate = birthDate;

};

Console.prototype.gameStyle = 'Console Gaming'; // inherit from this prototype

var nintendo = new Console('Nintendo','Japan',1889);
var xbox = new Console('Xbox','USA',2001);
var playstation = new Console('Playstation','Japan',1994);

//3rd method is via .create method - inherit from prototype

var consoleProto = { //this is not a function contructor, thus small starting letters
	calcAge: function(){
		console.log(2020 - this.birthDate); //to run the actual function have to type sega.calcAge();
	}
};

var sega = Object.create(consoleProto,
	{
		name: {value: 'Sega'},
		country: {value: 'Japan'},
		birthDate: {value: 1960}
	});


var age2;
var age = 27;
var obj = {
	name: 'jonas',
	city: 'lisbian'
};

function change(age,b){
	var age2 = age; //passing a primative inside never affects the variable in the outside we making a copy of age
	age2 += age2;
	b.city = 'Toronto'; // we are passing a reference to the object here
	return age2; //for that same reason - we are passing a brand new variable.
}
/*
change(age, obj);

console.log(age); // answer is 27, since we are passing a copy of age into the function.
console.log(obj.city); //answer is TORONTO
*/
console.log(age2); //undefined here
age2 = change(age, obj);
console.log(change(age, obj));
console.log(obj.city);
console.log(age2); //54 here. manipulate from function.

// First Class Function - Passing functions as arguments
	//My own function where computer part constrants

var budget = [2000,1750,1500,1000,800];

var newCalc = function(initial,fn) {
	var newArray = [];
	for(i=0; i<initial.length; i++){
		newArray.push(fn(initial[i])); //question could you call functions inside a function.
	}
	return newArray;
};

var compCost = function(e) {
	return e >= 1650;
};

console.log(newCalc(budget,compCost));

//IIFE - Data Privacy Function

(function(goodluck) {

	var score = Math.random()*10;
	console.log(score >= 5 - goodluck);

})(5); //passing parameter here


