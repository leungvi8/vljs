/////////////////////////////
// CODING CHALLENGE - JONAS SOLUTION


/*
--- Let's build a fun quiz game in the console! ---

1. Build a function constructor called Question to describe a question. A question should include:
a) question itself
b) the answers from which the player can choose the correct one (choose an adequate data structure here, array, object, etc.)
c) correct answer (I would use a number for this)

2. Create a couple of questions using the constructor

3. Store them all inside an array

4. Select one random question and log it on the console, together with the possible answers (each question should have a number) (Hint: write a method for the Question objects for this task).

5. Use the 'prompt' function to ask the user for the correct answer. The user should input the number of the correct answer such as you displayed it on Task 4.

6. Check if the answer is correct and print to the console whether the answer is correct ot nor (Hint: write another method for this).

7. Suppose this code would be a plugin for other programmers to use in their code. So make sure that all your code is private and doesn't interfere with the other programmers code (Hint: we learned a special technique to do exactly that).
*/

//Use IIFE to Privitize

// (function () {

	function Question (question,answers,correct){ //function contructor
		this.question = question;
		this.answers = answers;
		this.correct = correct;
	}

	Question.prototype.displayQuestion = function(){ //first prototype method - displayQuestion
		console.log(this.question);

		for (var i=0; i < this.answers.length; i++){
			console.log(i + ': ' + this.answers[i]);
		}
	}

	Question.prototype.checkAnswer = function(ans, callback){ //second prototype method - checkAnswer
			// console.log( + 'this');
			var parseAns = parseInt(ans);
			var sc;
			if (ans === 'exit'){
				console.log('exited thanks for playing');
			} else if (parseAns === this.correct){
				console.log('CORRECT ANSWER, play again');
				sc = callback(true);
				this.displayScore(sc);
				loop();
			} else {
				console.log('Wrong answer play again!');
				sc = callback(false);
				this.displayScore(sc);
				loop();
			}
			
	}

	Question.prototype.displayScore = function(score){
		console.log('You current score is ' + score);
		console.log('--------------------------------');
	}

	var guyQuestion = new Question('Am I a Guy?', ['0: yes', '1: no'], 0);
	var codeQuestion = new Question('Who wrote this code?', ['0: Kitty', '1: Zach', '2: Vince'], 2);
	var gameQuestion = new Question('What game genre do I enjoy playing most?', ['0: RPG', '1: FPS', '2: Sports'], 1);

	var questions = [guyQuestion,codeQuestion,gameQuestion]; //store all in array

	

	function score(){
		var sc = 0;
		return function(correct){ //true or false
			if (correct){
				sc++;
			}
			return sc;
		}
	}

	var keepScore = score();
	
	function loop (){

	var n = Math.floor(Math.random() * questions.length); //make random number (and round it using Math.floor);
	/*** Here is where Jonas answer is different ***/
		//He use the method of prototype and prototype chain - inheritance - notes 60
		//First he created a new prototype above to display console.

	questions[n].displayQuestion();

	var answer = prompt('Please select the correct answer'); //covert to number and not string

	questions[n].checkAnswer(answer, keepScore);
	}

	loop();

// })();

/*
--- Expert level ---

8. After you display the result, display the next random question, so that the game never ends (Hint: write a function for this and call it right after displaying the result)

9. Be careful: after Task 8, the game literally never ends. So include the option to quit the game if the user writes 'exit' instead of the answer. In this case, DON'T call the function from task 8.

10. Track the user's score to make the game more fun! So each time an answer is correct, add 1 point to the score (Hint: I'm going to use the power of closures for this, but you don't have to, just do this with the tools you feel more comfortable at this point).

11. Display the score in the console. Use yet another method for this.
*/