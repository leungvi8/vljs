	//Reasoning for creating module: keep related pieces of code together (inside independant units).
// Each of these module will have varibales and functions that are private, only accessible inside the modules.
//No other codes will be overrided. There will be private and public methods, this is called data encapsulation.

/* Working module with IIFE | Closures | Methods - Module breakdown */
/*var budgetController = (function(){

	var x = 23;
	var add = function(a){
		return x + a;
	}

	return {
		publicTest: function(b) {
			return add(b);
		}
	}

})();

var UIController = (function(){ //why is this one capital




})();

var controller = (function(budgetCtrl,UICtrl){

	var z = budgetCtrl.publicTest(5);

	return {
		anotherPublic: function(){
			console.log(z);
		}
	}

})(budgetController, UIController);
*/

/*Project

73 - First Event Listeners

*/

//Budget Controller (data)
var budgetController = (function(){

	var Expense = function(id, description, value){
		this.id = id;
		this.description = description;
		this.value = value;
	};

	var Income = function(id, description, value){
		this.id = id;
		this.description = description;
		this.value = value;
	};

	var calculateTotal = function(type){
		var sum = 0;
		data.allItems[type].forEach(function(cur){

			sum += cur.value;

		});
		data.totals[type] = sum;

	};


	var data = {
		allItems: {
			exp: [],
			inc: []
		},
		totals: {
			exp: 0,
			inc: 0
		},
		budget: 0,
		percentage: -1 //this means doesn't exist
	};

	return {
		addItem: function(type, des, val) { //description, value
			var newItem, ID;

            //[1 2 3 4 5], next ID = 6
            //[1 2 4 6 8], next ID = 9
            // ID = last ID + 1

			//create new id
			if (data.allItems[type].length > 0) {
				ID = data.allItems[type][data.allItems[type].length - 1].id + 1; //new ID for new item
			} else {
				ID = 0;
			}


			//create new item based on 'inc' or 'exp' type
			if (type === 'exp'){
				newItem = new Expense(ID, des, val);
			}  else if (type === 'inc') {
				newItem = new Income(ID, des, val);
			}

			//push it into our data structure
			data.allItems[type].push(newItem);

			//return the new element
			return newItem;

		},

		deleteItem: function(type, id){
			var ids, index;
			// id = 6
			// data.allItems[type][id]; DOES NOT WORK BECAUSE ITEMS ALWAYS GET SHIFTED
			// id = [1, 2, 4, 6, 8]
			// index = 3; THIRD PLACE IN THE ARRAY

			ids = data.allItems[type].map(function(current){
				
				return current.id;

			});

			index = ids.indexOf(id);

			if(index !== -1){
				data.allItems[type].splice(index, 1); //remove the item, 1 represents just removing 1 element.
			}

		},

		calculateBudget: function(){
			//calculate total income and expenses
			calculateTotal('exp');
			calculateTotal('inc');

			//calculate budget: income - expenses
			data.budget = data.totals.inc - data.totals.exp;

			//calculate the percentage of income that we spent
			if(data.totals.inc > 0){
				data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
					//e.g. Expense = 100 and income is 200, spent 50%; (100/200) = 0.5 * 100
			} else {
				data.percentage = -1;
			}

		},

		getBudget: function(){
			return {
				budget: data.budget,
				totalInc: data.totals.inc,
				totalExp: data.totals.exp,
				percentage: data.percentage
			};
		},

		testing: function() {
			console.log(data);
		}
	};

})();



//UI Controller
var UIController = (function(){ //why is this one capital

	var DOMstrings = {
		inputType: '.add__type',
		inputDescription: '.add__description',
		inputValue: '.add__value',
		inputBtn: '.add__btn',
		incomeContainer: '.income__list',
		expensesContainer: '.expenses__list',
		budgetLabel: '.budget__value',
		incomeLabel: '.budget__income--value',
		expenseLabel: '.budget__expenses--value',
		percentageLabel: '.budget__expenses--percentage',
		container: '.container'
	};

	return {
		getinput: function(){
			return {
			type : document.querySelector(DOMstrings.inputType).value, //will be either inc or exp
			description : document.querySelector(DOMstrings.inputDescription).value,
			value : parseFloat(document.querySelector(DOMstrings.inputValue).value) //converted string to number
			};
		},

		addListItem: function(obj,type){

			var html, newHtml, element;
			// Create HTML string with placeholder text (taken from our index.html). Using %SOMETHING% than it's easier to find where placeholder text is.

			if ( type === 'inc'){
				element = DOMstrings.incomeContainer;

            	html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
        	} else if (type === 'exp') {
        		element = DOMstrings.expensesContainer;

				html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
			}

			// Replace placeholder text with some actual data
			newHtml = html.replace('%id%', obj.id);
			newHtml = newHtml.replace('%description%', obj.description);
			newHtml = newHtml.replace('%value%', obj.value);

			// Insert HTML into the DOM
			document.querySelector(element).insertAdjacentHTML('beforeBegin', newHtml);

		},

		deleteListItem: function(selectorID){

			var el = document.getElementById(selectorID)
			el.parentNode.removeChild(el);

		},

		clearFields: function(){
			var fields, fieldsArr;

			fields =  document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);

			fieldsArr = Array.prototype.slice.call(fields); //convert to array

			fieldsArr.forEach(function(current, index, array) {
				current.value = "";
			});

			fieldsArr[0].focus(); //Clear above than refocus to Description box

		},

		displayBudget: function(obj){

			document.querySelector(DOMstrings.budgetLabel).textContent = obj.budget;
			document.querySelector(DOMstrings.incomeLabel).textContent = obj.totalInc;
			document.querySelector(DOMstrings.expenseLabel).textContent = obj.totalExp;


			if (obj.percentage > 0){
				document.querySelector(DOMstrings.percentageLabel).textContent = obj.percentage + '%';
			} else {
				document.querySelector(DOMstrings.percentageLabel).textContent = '---';
			}


		},

		getDOMstrings: function(){
			return DOMstrings;
		}
	};

})();



//Global App Controller
var controller = (function(budgetCtrl,UICtrl){

	var setupEventListeners = function(){
		var DOM = UICtrl.getDOMstrings();

		// document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem); //callback

		document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

		document.addEventListener('keypress', function(event){
			if (event.keyCode === 13 || event.which === 13){
				console.log('fired');
				ctrlAddItem();

			}
		});

		document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);
	};

	/*vvvvvvvvvvvvv MOVED #5 and #6 HERE TO NEW FUNCTION vvvvvvvvvvvvv*/

	var updateBudget = function(){

		// 5. Cacluate the budget
		budgetCtrl.calculateBudget();

		// 6. Return the budget
		var budget = budgetCtrl.getBudget();

		// 7. Display the budget on the UI
		UICtrl.displayBudget(budget);


	};


	var ctrlAddItem = function() {
		var input, newItem;

		// 1. Get the filled input data
		input = UICtrl.getinput();
		console.log(input);

		if(input.description !== "" && !isNaN(input.value) && input.value > 0){
			// 2. Add the item to the budget controller
			newItem = budgetCtrl.addItem(input.type, input.description, input.value);
			console.log(newItem);
			// 3. Add the item to the UI
			UICtrl.addListItem(newItem, input.type);

			// 4. Clear the Fields
			UICtrl.clearFields();

			/*^^^^^^^^^^^^^^^^WERE GOING TO MOVE THESE TWO TO SEPERATE FUNCTION TO AVOID DRY AS WE NEED TO USE THIS FUNCTION MULTIPLE TIMES = MOVE ABOVE^^^^^^^^^*/

			// #. Cacluate the budget

			// #. Return the budget

			// #. Display the budget on the UI

			/*-------------------------------------------------------*/

			// 5. Calculate and update budget
			updateBudget();

		} else {
			alert('Some of your inputs is not correct');
		}

	};

	var ctrlDeleteItem = function(event) {

		var itemID, splitID, type, ID;
		
		itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;

		if(itemID) {

			//inc-1
			splitID = itemID.split('-'); //splitting thet string to catch just the number
			type = splitID[0]; //Exp or Inc
			ID = parseInt(splitID[1]); //the Specific ID Number

			//1. Delete the item from the data structure
			budgetCtrl.deleteItem(type, ID);

			//2. Delete the item from the user interface
			UICtrl.deleteListItem(itemID);

			//3. Update and show the new budget
			updateBudget();

		} 	

	};

	return {
		init: function(){
			console.log('Application Started.');
			UICtrl.displayBudget({
				budget: 0,
				totalInc: 0,
				totalExp: 0,
				percentage: -1
			});
			setupEventListeners();
		}
	};

})(budgetController, UIController);

controller.init();




